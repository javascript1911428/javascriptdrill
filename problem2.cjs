function Problem2(inventory){
    let last_car = inventory[0];
    let last_car_year = last_car.car_year;
    for(let i in inventory){
        if(last_car_year < inventory[i].car_year){
            last_car_year = inventory[i].car_year;
            last_car = inventory[i];
        }
    }

    return last_car
}

module.exports = Problem2;