function Problem1(inventory,id0) {
    if (typeof(inventory) == "object" && inventory != null && id0 != null) {
        if (!Array.isArray(inventory)) {
            return [];
        }
        for (let i = 0; i < inventory.length; i++){
            if(inventory[i].id == id0){
                return inventory[i];
            }
        }
    }

    return [];
}
module.exports = Problem1;
